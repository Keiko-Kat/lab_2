#include <string>
#include <iostream>
using namespace std;


class Pila{
	private:
		int tope;
		bool band;
		int max;
		int pila;
	public:
		Pila();
		Pila(int tope, bool band, int max, int, pila);
		
		void go_tope(int tope); 
		
		//set tope se utiliza en el main partiendo con tope = -1, de ahi se modifica//
		void set_tope(int tope, int max, int push_pop);
		
		//band depende del tope, su valor inicial será true//
		void set_band(int tope);
		
		//el maximo se le pedira al usuario//
		void set_max(int max);
		
		//la pila seŕa un array de tamaño max//
		void set_pila(int max);
		
		//tanto tope y max se manejarán como int simples//
		int get_tope();
		int get_max();
		
		//pila se llenara con una funcion interna//
		void fill_pila(int tope, int numb);
		
		//y se llamara de una forma similar//
		int see_pila(int tope);
		
		//band imprimira mensajes cuando se pida//
		bool get_band();
};
