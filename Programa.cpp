#include <string>
#include <iostream>
using namespace std;
#include "Pilas.cpp"


void print_pila(Pila pila){
	//Se imprime un "titulo" primero::
	cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
	cout<<"~~~~~~~~~~~~~~Pila~~~~~~~~~~~~~~"<<endl;
	//luego se recorre la pila desde la posicion "tope" hasta 0"
	
	for(int i = pila.get_tope(); i >= 0; i--){
		cout<<"................................"<<endl;
		cout<<"              ["<< pila.see_pila(i) <<"]   "<<endl;
	}
	cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl; 
}

int main(){
	//inicio se crea como "0" para que "comience" el programa//
	int inicio = 0, numero, in_out;
	string mar, num;
		
	//se le pide el tamaño de la pila al usuario//
	cout << "Ingrese cantidad Máxima de la pila: ";
	getline(cin, num);
	numero = stoi(num);	
	
	//se genera pila//
	Pila pila;
	
	//se inicializan los atributos básicos//
	pila.go_tope(0);
	pila.set_max(numero);
	pila.set_pila(pila.get_max());
	//se ingresa a un while que funcionara hasta la orden de salida "0"//
	while (inicio != 0){
			
		pila.set_band(pila.get_tope());
		//band debe renovarse en cada iteración//
		//como primera accion se imprime el menú//	
		cout<<"Push~~~~~~~~~~~~~~[1]"<<endl;
		cout<<"Pop~~~~~~~~~~~~~~~[2]"<<endl;	
		cout<<"Ver Pila~~~~~~~~~~[3]"<<endl;
		cout<<"Salir~~~~~~~~~~~~~[4]"<<endl;
		cout<<"~~~~~~~~~~~~~~~~~~~~~"<<endl;
		cout<<"Opción:  ";
		
		//se pide la opción//
		getline(cin, mar);
		inicio = stoi(mar);
		
		//y se abre un switch dependiente de inicio//
		
		switch(inicio){
			case 1:
				//si se escoge la opcion push se puede verificar el valor max vs tope// 
				if (pila.get_tope() < pila.get_max()){
					
					//si hay espacio se pide el valor que tomara la nueva posicion tope//
					cout << "Ingrese valor: ";
					getline(cin, mar);
					in_out = stoi(mar);
					
					pila.set_tope(pila.get_tope(), pila.get_max(), inicio);
					//se añade el valor ingresado in_out a la pila//
					pila.fill_pila(pila.get_tope(), in_out);
					
					
				}break;
				  
			case 2:				
				//si se escoge la opcion pull se puede verificar el valor band// 
				pila.get_band();
				//luego se lo ingresa en un if//
				if (pila.get_band() == false ){
					
					//Se modifica el valor tope igualandolo a 0//
					pila.fill_pila(pila.get_tope(), 0);
					
					//luego se modifica el valor de tope para "eliminar" el valor//
					pila.set_tope(pila.get_tope(), pila.get_max(), inicio);		
				
				}break;
				
			case 3:
				print_pila(pila);
				break;
			
			case 0: break;
			
			default: cout<< "~~~~~~~~~Ingreso invalido~~~~~~~"<<endl; break;
		}		
	}
	return 0;
}
